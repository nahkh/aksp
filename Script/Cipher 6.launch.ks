PRINT "Executing launch procedure for Cipher 6.".
PRINT "Mission: Establish orbit at 100km. Establish rendezvous with the Mun. Return to Kerbin.".

DOWNLOAD("ascent.ks").

RUN ascent.ks.

LAUNCH(100000, 90).

DELETE ascent.ks.

WAIT UNTIL ADDONS:RT:HASCONNECTION(SHIP).

DOWNLOAD("orbit.ks").

PRINT "Standing by for further instructions.".
