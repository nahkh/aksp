// Mission to the mun and back

DOWNLOAD("orbit.ks").
run orbit.

PRINT "Calculating values for munar transfer.".

LOCAL transferAltitude IS MUN:ALTITUDE - (MUN:RADIUS + MUN:SOIRADIUS)/2.
LOCAL hohmann IS CALC_HOHMANN_TRANSFER(SHIP:ALTITUDE, transferAltitude, KERBIN).
PRINT "Transfer time to the Mun: " + hohmann[2].
PRINT "Transfer angle " + CALC_INSERTION_ANGLE(MUN, hohmann[2]).
