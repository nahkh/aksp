REQUIRE("manouver.ks").
RUN manouver.

FUNCTION WAIT_ORBITS {
  PARAMETER orbits.
  PRINT "Waiting for " + orbits + " orbits.".
  LOCAL targetTime IS TIME + orbits * SHIP:OBT:PERIOD.
  WAIT UNTIL TIME > targetTime.
}

FUNCTION WAIT_UNTIL_BURN {
  PARAMETER targetLongitude.
  PARAMETER deltaV.
  LOCAL time_for_burn IS ESTIMATE_TIME_FOR_BURN(deltaV).
  WAIT UNTIL MOD(SHIP:LONGITUDE + 360*time_for_burn/SHIP:OBT:PERIOD, 360) > targetLongitude.
}

FUNCTION CALC_DEGREES_AHEAD {
  PARAMETER orbitable1.
  PARAMETER orbitable2.
  RETURN MOD(360 + orbitable2:LONGITUDE - orbitable1:LONGITUDE, 360).
}

FUNCTION GET_ORBITAL_VELOCITY {
  PARAMETER orbitingBody.
  PARAMETER a.
  PARAMETER currentAltitude.
  IF a < 0.1 {
    PRINT "Invalid input for GET_ORBITAL_VELOCITY".
    RETURN 0.
  }
  RETURN SQRT(orbitingBody:MU * (2 / (orbitingBody:RADIUS + currentAltitude) - 1 / a)).
}

FUNCTION CIRCULARIZE_AT_APOAPSIS {
  PRINT "Circularizing at apoapsis.".
  WAIT UNTIL ETA:APOAPSIS < 10.
  LOCAL target_altitude IS SHIP:OBT:APOAPSIS.
  LOCK STEERING TO PROGRADE.
  WAIT 10.
  LOCK THROTTLE TO 1.0.
  WAIT UNTIL SHIP:OBT:PERIAPSIS > 0.95 * target_altitude.
  LOCK THROTTLE TO 0.0.
}

FUNCTION CALC_HOHMANN_TRANSFER {
  PARAMETER oldAltitude.
  PARAMETER newAltitude.
  PARAMETER currentBody.
  LOCAL radiusA IS SHIP:BODY:RADIUS + oldAltitude.
  LOCAL radiusB IS SHIP:BODY:RADIUS + newAltitude.
  LOCAL transferA IS (radiusA + radiusB) / 2.
  LOCAL velocityA IS GET_ORBITAL_VELOCITY(currentBody, radiusA, oldAltitude).
  LOCAL velocityB IS GET_ORBITAL_VELOCITY(currentBody, radiusB, newAltitude).
  LOCAL transferVelocityA IS GET_ORBITAL_VELOCITY(currentBody, transferA, oldAltitude).
  LOCAL transferVelocityB IS GET_ORBITAL_VELOCITY(currentBody, transferA, newAltitude).
  LOCAL firstBurn IS transferVelocityA - velocityA.
  LOCAL secondBurn IS velocityB - transferVelocityB.
  RETURN LIST(firstBurn, secondBurn, CALC_PERIOD(currentBody, transferA)/2).
}

FUNCTION CALC_PERIOD {
  PARAMETER currentBody.
  PARAMETER semiMajorAxis.
  RETURN 2*CONSTANT():PI*SQRT((semiMajorAxis^3)/currentBody:MU).
}

FUNCTION CALC_INSERTION_ANGLE {
  PARAMETER orbitable.
  PARAMETER transferTime.
  return 360*transferTime/orbitable:OBT:PERIOD.
}

//LOCAL hohmann IS CALC_HOHMANN_TRANSFER(ALTITUDE, newAltitude, SHIP:OBT:BODY).
FUNCTION EXEC_HOHMANN_TRANSFER {
  PARAMETER hohmann.
  PRINT "Executing Hohmann transfer, first burn = " + hohmann[0] + " second burn = " + hohmann[1] + ", transfer time = " +hohmann[2].
  LOCAL firstBurn IS hohmann[0].
  LOCAL secondBurn IS hohmann[0].
  INSERTION(firstBurn, 40).
  WAIT UNTIL ETA:PERIAPSIS < 40.
  INSERTION(secondBurn, 40).
}
