// Manouver library v0.0.1.
// Juuso Valli

PRINT "Loading manouver.ks version 0.0.1".

LOCAL previousMax IS "unset".

FUNCTION DEORBIT {
  PARAMETER immediate.
  PRINT "Performing deorbit".
  IF NOT immediate {
    PRINT "Coasting to apoapsis.".
    WAIT UNTIL ETA:APOAPSIS < 20.
  } ELSE {
    PRINT "Deorbiting without delay.".
  }
  LOCK STEERING TO RETROGRADE.
  WAIT 20.
  LOCK THROTTLE TO 1.0.
  UNTIL SHIP:OBT:PERIAPSIS < 25000 {
    STAGE_IF_NEEDED().
    WAIT 0.1.
  }
  LOCK THROTTLE TO 0.0.
  WAIT 1.
  STAGE.
  STAGE.
  STAGE.
  LOCAL oldAltitude IS ALTITUDE.
  UNTIL NEAR_VALUE(oldAltitude, ALTITUDE, 1) {
    SET oldAltitude TO ALTITUDE.
    WAIT 1.
  }
}

FUNCTION STAGE_IF_NEEDED {
	IF previousMax = "unset" {
		SET previousMax TO MAXTHRUST.
	}
	IF previousMax > MAXTHRUST + 10 {
		LOCAL oldThrottle IS THROTTLE.
		LOCK THROTTLE TO 0.
		WAIT 1. STAGE. WAIT 1.
		LOCK THROTTLE TO oldThrottle.
		SET previousMax TO MAXTHRUST.
	}
}

FUNCTION CALC_FUEL_FLOW {
  LOCAL fuelFlow IS 0.
  LIST ENGINES IN shipEngines.
  FOR engine IN shipEngines {
    SET fuelFlow TO fuelFlow + (engine:MAXTHRUST / (engine:ISP * 9.81)).
  }
  PRINT "Ship fuel flow is " + fuelFlow.
  RETURN fuelFlow.
}

FUNCTION CALC_DELTA_MASS_FOR_DV {
  PARAMETER dv.
  PARAMETER isp.
  PARAMETER currentMass.
  RETURN currentMass * (1 - (1/(CONSTANT():E ^ (dv/(9.81*isp))))).
}

FUNCTION CALC_EXACT_TIME_FOR_BURN {
  PARAMETER dv.
  LIST ENGINES IN shipEngines.
  LOCAL maxThrust IS 0.
  LOCAL assumedISP IS 0.
  FOR engine IN shipEngines {
    IF engine:IGNITION {
      IF engine:MAXTHRUST > maxThrust {
        SET maxThrust TO engine:MAXTHRUST.
        SET assumedISP to engine:ISP.
      }
    }
  }
  IF dv < 0 {
    SET dv TO -dv.
  }
  LOCAL massToBurn IS CALC_DELTA_MASS_FOR_DV(dv, assumedISP, SHIP:MASS).
  PRINT "DEBUG need to burn " + massToBurn + " tons of fuel.".
  LOCAL fuelFlow IS CALC_FUEL_FLOW().
  IF fuelFlow < 0.001 {
    PRINT "Fuel flow is 0, can't compute time for burn.".
    RETURN 0.
  }
  RETURN massToBurn / fuelFlow.
}

// Execute orbital insertion
FUNCTION INSERTION {
  PARAMETER deltaV.
  PARAMETER timeOffset.
  PRINT "Executing insertion dv " + deltaV + " with burn center in " + timeOffset + "s".
  SET centerTime TO TIME + timeOffset.
  IF NEAR_VALUE(deltaV, 0, 5) {
    // Don't use main engines for this
    RETURN.
  }
  LOCAL burnTime IS CALC_EXACT_TIME_FOR_BURN(deltaV).
  LOCAL targetHeading IS PROGRADE.
  LOCAL reverse IS deltaV < 0.
  IF reverse {
    SET targetHeading TO RETROGRADE.
  }
  LOCK STEERING TO targetHeading.
  LOCAL startTime IS centerTime - burnTime / 2.
  PRINT "Executing " + burnTime + "s burn in " + (startTime - TIME) + "s".
  WAIT UNTIL TIME > startTime.
  LOCK THROTTLE TO 1.0.
  WAIT UNTIL TIME > startTime + burnTime.
  LOCK THROTTLE TO 0.0.
}
