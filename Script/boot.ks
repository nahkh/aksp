// Boot script v0.0.2
// Juuso Valli
PRINT " ".

CLEARSCREEN.

PRINT "Boot script 0.0.2".
PRINT SHIP:NAME.

SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

FUNCTION HAS_FILE {
	PARAMETER name.
	PARAMETER vol.

	SWITCH TO vol.
	LIST FILES IN allFiles.
	FOR file IN allFiles {
		IF file:NAME = name {
			SWITCH TO 1.
			RETURN TRUE.
		}
	}
	SWITCH TO 1.
	RETURN FALSE.
}

FUNCTION DOWNLOAD {
	PARAMETER name.
	if HAS_FILE(name, 0) {
		IF HAS_FILE(name, 1) {
			DELETE name.
		}
		COPY name FROM 0.
	}
}

FUNCTION UPLOAD {
	PARAMETER name.
	IF HAS_FILE(name, 1) {
		IF HAS_FILE(name, 0) {
			SWITCH TO 0. DELETE name. SWITCH TO 1.
		}
		COPY name TO 0.
	}
}

FUNCTION SET_STARTUP{
	PARAMETER filename.
	IF HAS_FILE(filename, 0) {
		IF HAS_FILE("startup.ks", 1) {
			DELETE startup.ks.
		}
		DOWNLOAD(filename).
		RENAME filename to "startup.ks".
	}
}

FUNCTION REQUIRE {
	PARAMETER filename.
	IF NOT HAS_FILE(filename, 1) {
		IF HAS_FILE(filename, 0) {
			DOWNLOAD(filename).
		}
	}
}

FUNCTION NEAR_VALUE {
	PARAMETER value1.
	PARAMETER value2.
	PARAMETER epsilon.
	RETURN ABS(value1 - value2) < epsilon.
}

FUNCTION AT_KSC {
	RETURN SHIP:BODY:NAME = "Kerbin" AND NEAR_VALUE(SHIP:LONGITUDE, -75, 1) AND NEAR_VALUE(SHIP:LATITUDE, 0, 1) AND NEAR_VALUE(SHIP:ALTITUDE, 0, 200).
}

FUNCTION CONTROLLABLE {
	RETURN TRUE. //ADDONS:RT:HASCONNECTION(SHIP) OR ADDONS:RT:HASLOCALCONTROL(SHIP).
}


// Read instructions from server
SET updateScript TO SHIP:NAME + ".update.ks".
SET launchScript TO SHIP:NAME + ".launch.ks".
SET finalLaunchScript To "launch.ks".

//PRINT "Expected launch script called " + launchScript.
//PRINT "Expected update script called " + updateScript.

// Launch
IF CONTROLLABLE() {
	PRINT "Connection established to KSC.".
	IF(AT_KSC()) {
		CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").
		PRINT "On the launch pad. Checking for launch script.".
		IF HAS_FILE(launchScript, 0) {
			PRINT "Downloading launch script".
			DOWNLOAD(launchScript).
			IF HAS_FILE(finalLaunchScript, 1) {
				delete launch.ks.
			}
			RENAME launchScript to finalLaunchScript.
		}
		IF HAS_FILE(finalLaunchScript, 1) {
			run launch.ks.
			PRINT "Launch executed. Deleting local file to save space".
			delete launch.ks.
		}
	}

}


// Load updates
IF ADDONS:RT:HASCONNECTION(SHIP) {

	IF(HAS_FILE(updateScript, 0)) {
		PRINT "Found update script, executing.".
		DOWNLOAD(updateScript).
		SWITCH TO 0. DELETE updateScript. SWITCH TO 1.
		IF HAS_FILE("update.ks", 1) {
			delete update.ks.
		}
		RENAME updateScript TO "update.ks".
		run update.ks.
		PRINT "Executed update script.".
		delete update.ks.
	}
}

IF HAS_FILE("startup.ks", 1) {
	PRINT "Executing startup script.".
	RUN startup.ks.
} ELSE {
	PRINT "No startup script found, rebooting in 10s.".
	wait until addons:rt:hasconnection(ship).
	wait 10.
	reboot.
}
