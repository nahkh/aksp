PRINT "Executing launch procedure for Cipher 3.".

DOWNLOAD("ascent.ks").
DOWNLOAD("orbit.ks").

RUN ascent.ks.
RUN orbit.ks.

LAUNCH(80000, 90).
WAIT_ORBITS(1).
DEORBIT(FALSE).
