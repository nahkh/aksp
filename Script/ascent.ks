// Ascent v0.0.5
// Juuso Valli

REQUIRE("manouver.ks").
RUN manouver.
PRINT "Loading ascent script version 0.0.5".

FUNCTION ASCENT_ANGLE {
	PARAMETER current_altitude.
	PARAMETER target_altitude.
	LOCAL relative_altitude IS MAX(0, MIN(1, current_altitude/target_altitude)).
	RETURN 90 * (1 / (1 + ABS(relative_altitude))).
}

FUNCTION TWR {
	RETURN SHIP:AVAILABLETHRUST / (9.81 * SHIP:MASS).
}

FUNCTION CALC_THROTTLE {
	PARAMETER desiredTWR.
	LOCAL maxTWR IS TWR().
	IF TWR < 0.01 {
		// Preventing Div by zero.
		RETURN 0.
	}
	RETURN MAX(0, MIN(1, desiredTWR/maxTWR)).
}

FUNCTION LAUNCH {
	PARAMETER target_altitude.
	PARAMETER direction.

	PRINT "Launching to altitude " + target_altitude + ", direction " + direction + ".".
	PRINT "Initializing controls".
	LOCAL ascent_heading IS HEADING(direction, ASCENT_ANGLE(SHIP:ALTITUDE, target_altitude)).
	LOCAL throttleSetting IS 1.0.
	LOCK STEERING TO ascent_heading.
	SAS ON.
	SET SASMODE TO "STABILITYASSIST".
	PRINT "Main throttle up.".
	LOCK THROTTLE TO throttleSetting.
	PRINT "Counting down".
	FROM {local countdown is 5.} UNTIL countdown = 0 STEP {SET countdown to countdown -1.} DO {
		PRINT "..." + countdown.
		WAIT 1.
	}
	// LAUNCH
	PRINT "Liftoff.".
	STAGE.

	LOCAL apo_is_not_close IS TRUE.
	LOCAL throttleModifier IS 1.

	UNTIL SHIP:OBT:APOAPSIS > target_altitude {
		SET ascent_heading TO HEADING(direction, ASCENT_ANGLE(SHIP:ALTITUDE, target_altitude)).
		STAGE_IF_NEEDED().
		IF apo_is_not_close AND SHIP:OBT:APOAPSIS > target_altitude*0.95 {
			SET throttleModifier TO 0.1.
			SET apo_is_not_close TO FALSE.
		}
		// With throttle limiting
		// SET throttleSetting TO CALC_THROTTLE(2) * throttleModifier.
		// Without throttle limiting.
		SET throttleSetting TO throttleModifier.
		WAIT 0.1.
	}

	PRINT "Cutting throttle, coasting to apoapsis".
	LOCK THROTTLE TO 0.0.

	WAIT UNTIL ETA:APOAPSIS < 40.
	LOCK STEERING TO HEADING(direction, 0).
	WAIT 20.
	PRINT "Near apoapsis, performing orbital insertion burn.".

	SET ascent_heading TO PROGRADE.

	TOGGLE LIGHTS.

	SET throttleModifier to 1.0.
	LOCAL peri_is_not_close IS TRUE.
	SET ascent_heading TO HEADING(direction, 0).
	LOCAL offset IS 0.
	LOCAL last_apoapsis IS SHIP:OBT:APOAPSIS.
	LOCK STEERING TO ascent_heading.
	LOCK THROTTLE TO throttleSetting.
	// FIXME
	UNTIL SHIP:OBT:PERIAPSIS > 70000 {
		STAGE_IF_NEEDED().
		IF peri_is_not_close AND SHIP:OBT:PERIAPSIS > target_altitude*0.95 {
			SET throttleModifier TO 0.1.
			SET peri_is_not_close TO FALSE.
		}
		IF ETA:PERIAPSIS < ETA:APOAPSIS {
			// We're past apoapsis
			IF last_apoapsis < SHIP:OBT:APOAPSIS {
				SET offset TO offset + 1.
			}
		}
		SET last_apoapsis TO SHIP:OBT:APOAPSIS.

		SET ascent_heading TO heading(direction, offset).
		SET throttleSetting TO throttleModifier.
		WAIT 0.1.
	}
	PRINT "Orbit achieved.".
	LOCK THROTTLE TO 0.0.
	WAIT 2.
}
