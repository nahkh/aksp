PRINT "Executing launch procedure for Cipher 5.".
PRINT "Mission: Establish orbit at 100km. Perform Hohmann transfer to 200km. Deorbit.".

DOWNLOAD("ascent.ks").
DOWNLOAD("orbit.ks").

RUN ascent.ks.
RUN orbit.ks.

LAUNCH(100000, 90).
CIRCULARIZE_AT_APOAPSIS().
EXEC_HOHMANN_TRANSFER(200000).
DEORBIT(TRUE).
