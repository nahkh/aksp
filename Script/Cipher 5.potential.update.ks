PRINT "Executing update for Cipher 5.".
PRINT "Mission: Establish orbit at 100km. Perform Hohmann transfer to 200km. Deorbit.".

DOWNLOAD("orbit.ks").
DOWNLOAD("ascent.ks").
RUN ascent.ks.
RUN orbit.ks.

DEORBIT(TRUE).
